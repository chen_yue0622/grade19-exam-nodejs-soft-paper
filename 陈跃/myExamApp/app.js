'use strict'
let Koa=require('koa');
let bodyparser=require('koa-bodyparser');

let controllers=require('./controllers');
let temp=require('./templating');
let rstatics=require('koa-static');

let app=new Koa();
app.use(bodyparser());
app.use(controllers());
app.use(temp);
app.use(rstatics(__dirname+'/static'));


let port=4000;
app.listen(port);
console.log(`http://127.0.0.1:${port}`);
