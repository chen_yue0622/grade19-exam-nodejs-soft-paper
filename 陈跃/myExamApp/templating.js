'use strict'
let nunjucks=require('nunjucks');
function create(path,opts){
    path=path || 'view';
    opts=opts || {};
    let confOpts={
        autoescape:opts.autoescape || true,
        throwOnUndefined:opts.throwOnUndefined || false,
        lstripBlocks:opts.lstripBlocks || false,
        trimBlocks:opts.trimBlocks || false,
        watch:opts.watch || true,
        noCache:opts.watch || true
    }
    let evn=nunjucks.configure(path,confOpts);
    return evn;
}

module.exports=async(ctx,next)=>{
       ctx.render=function(view,model){
            let evn=create();
            ctx.body=evn.render(view,model);
       }
       await next();
}