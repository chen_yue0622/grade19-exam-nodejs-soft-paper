'use strict'

let path = require('path');
let fs = require('fs');
let router = require('koa-router')();

function aaa(dir) {
    let files = fs.readdirSync(dir);
    let controllerFile = files.filter((name) => {
        return name.endsWith('.js') && name !== 'index.js';
    })
    return controllerFile;
}

function bbb(files) {
    files.forEach(item=>{
        let tmpPath=path.join(__dirname,item);
        let route=require(tmpPath);
        for(let key in route){
            let type=route[key][0];
            let fn=route[key][1];
            if(type==='get'){
                router.get(key,fn)
            }else{
                router.post(key,fn)
            }   
        }
    })
}

module.exports = function (currentDir) {
    let dir = currentDir || __dirname;
    let controllersFile = aaa(dir);
    bbb(controllersFile);
    return router.routes();
}